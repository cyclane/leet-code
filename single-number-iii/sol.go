package main

func singleNumber(nums []int) []int {
	xor := 0
	for i := range nums {
		xor ^= nums[i]
	}
	xor &= -xor
	out := []int{0, 0}
	for i := range nums {
		if nums[i]&xor == 0 {
			out[0] ^= nums[i]
		} else {
			out[1] ^= nums[i]
		}
	}
	return out
}
