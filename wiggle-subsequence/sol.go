package main

func wiggleMaxLength(nums []int) int {
	// Current's value only matters in terms of negative / 0 / positive.
	// The numerical value doesn't get used and therefore can be messed with.
	current, count := 0, len(nums)
	for i := 0; i < len(nums)-1; i++ {
		new := nums[i+1] - nums[i]
		if (current > 0 && new > 0) || (current < 0 && new < 0) || new == 0 {
			count--
		}
		if new != 0 {
			current = new
		}
	}
	return count
}
