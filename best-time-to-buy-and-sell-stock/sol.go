package main

// Time: O(n)
// Space: O(1)
func maxProfit(prices []int) int {
	min, max := prices[0], 0
	for _, price := range prices {
		if price < min {
			min = price
		} else if diff := price - min; diff > max {
			max = diff
		}
	}
	return max
}
