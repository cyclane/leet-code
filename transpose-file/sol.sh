#!/bin/sh

lines=$(grep -oP '(?<![^ \n])[^ \n]+(?![^ \n])' file.txt)
n=$(sed "1q;d" file.txt | wc -w)
top=$(expr $n - 1)
for i in $(seq 1 $top); do
	echo $(echo $lines | tr " " "\n" | awk "NR % $n == $i")
done
echo $(echo $lines | tr " " "\n" | awk "NR % $n == 0")
