package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidSubBST(root *TreeNode, min, max int) bool {
	if root == nil {
		return true
	}
	if root.Val < min || root.Val > max {
		return false
	}
	return isValidSubBST(root.Left, min, root.Val-1) && isValidSubBST(root.Right, root.Val+1, max)
}
func isValidBST(root *TreeNode) bool {
	return isValidSubBST(root, -1<<31, 1<<31)
}
