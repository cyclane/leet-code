package main

// Time: O(n)
// Space: O(n)
func containsDuplicate(nums []int) bool {
	exists := make(map[int]bool)
	for _, num := range nums {
		if _, ok := exists[num]; ok {
			return true
		}
		exists[num] = true
	}
	return false
}
