package main

var MIN, MAX = -(1 << 31), (1 << 31) - 1

func myAtoi(s string) int {
	m, n, c := 1, true, 0
	for _, char := range s {
		if n && char == ' ' {
			continue
		} else if n && char == '-' {
			m = -1
		} else if !(n && char == '+') {
			char_n := int(char - '0')
			if char_n < 0 || char_n > 9 {
				break
			}
			c *= 10
			c += char_n
			if c < 0 || c > MAX {
				if m == 1 {
					return MAX
				}
				return MIN
			}
		}
		n = false
	}
	return c * m
}
