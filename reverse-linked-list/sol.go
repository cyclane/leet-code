package main

type ListNode struct {
	Val  int
	Next *ListNode
}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *ListNode) *ListNode {
	var last *ListNode = nil
	current := head
	for current != nil {
		next := current.Next
		current.Next = last
		last = current
		current = next
	}
	return last
}
