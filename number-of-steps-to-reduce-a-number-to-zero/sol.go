package main

func numberOfSteps(num int) (steps int) {
	for num != 1 {
		if num%2 == 0 {
			num /= 2
		} else {
			num--
		}
		steps++
	}
	return steps
}
