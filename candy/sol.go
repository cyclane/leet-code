package main

func candy(ratings []int) int {
	count := 1
	for idx := 0; idx < len(ratings)-1; {
		for idx < len(ratings)-1 && ratings[idx+1] == ratings[idx] {
			count++
			idx++
		}
		next_inc := 1
		for idx < len(ratings)-1 && ratings[idx+1] > ratings[idx] {
			next_inc++
			idx++
			count += next_inc
		}
		next_dec := 1
		for idx < len(ratings)-1 && ratings[idx+1] < ratings[idx] {
			count += next_dec
			next_dec++
			idx++
		}
		if next_dec > next_inc {
			count += next_dec - next_inc
		}
	}
	return count
}
