package main

func mask(str string) int {
	mask := 0
	for _, r := range str {
		mask |= 1 << (r - 'a')
	}
	return mask
}

func findNumOfValidWords(words []string, puzzles []string) []int {
	word_masks := make(map[int]int)
	for _, word := range words {
		mask := mask(word)
		word_masks[mask] += 1
	}

	out := make([]int, len(puzzles))
	for pi, puzzle := range puzzles {
		pc := mask(puzzle[1:])
		fc := 1 << (puzzle[0] - 'a')
		count, submask := word_masks[fc], pc
		for submask != 0 {
			count += word_masks[submask|fc]
			submask = (submask - 1) & pc
		}
		out[pi] = count
	}
	return out
}
