package main

// Time: O(n)
// Space: O(1)
func maxSubArray(nums []int) int {
	current, max := 0, nums[0]
	for _, num := range nums {
		current += num
		if current > max {
			max = current
		}
		if current < 0 {
			current = 0
		}
	}
	return max
}
