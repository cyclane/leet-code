package main

import "sort"

func minDeletions(s string) int {
	freq := make([]int, 26) // only lower-case english letters
	for _, chr := range s {
		freq[chr-'a']++
	}
	sort.Ints(freq)

	count, max := 0, len(s)
	for idx := len(freq) - 1; idx >= 0 && freq[idx] != 0; idx-- {
		f := freq[idx]
		if f > max {
			count += f - max
			f = max
		}
		if f > 0 {
			max = f - 1
		} else {
			max = 0
		}
	}
	return count
}

func main() {
	println(minDeletions("bbcebab"))
}
