impl Solution {
    pub fn rotate(matrix: &mut Vec<Vec<i32>>) {
        let n = matrix.len() - 1;
        for y in 0..matrix.len()/2 {
            for x in y..matrix.len()-1-y {
                let mut a = matrix[y][x];
                matrix[y][x] = matrix[n-x][y];
                let mut b = matrix[x][n-y];
                matrix[x][n-y] = a;
                a = matrix[n-y][n-x];
                matrix[n-y][n-x] = b;
                matrix[n-x][y] = a;
            }
        }
    }
}