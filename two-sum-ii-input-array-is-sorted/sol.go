package main

// Time: O(n)
// Space: O(1)
func twoSum(numbers []int, target int) []int {
	p1, p2 := 0, len(numbers)-1
	for p1 < p2 {
		s := numbers[p1] + numbers[p2]
		if s > target {
			p2--
		} else if s < target {
			p1++
		} else {
			return []int{p1 + 1, p2 + 1}
		}
	}
	return []int{}
}
