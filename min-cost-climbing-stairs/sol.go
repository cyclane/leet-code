package main

// Basically a slightly altered,
// more hard-coded version of jump-game-vi
func minCostClimbingStairs(cost []int) int {
	last := [2]int{0, 0}
	for _, c := range cost {
		if last[0] < last[1] {
			last[0], last[1] = last[1], last[0]+c
		} else {
			last[0], last[1] = last[1], last[1]+c
		}
	}
	if last[0] < last[1] {
		return last[0]
	}
	return last[1]
}
