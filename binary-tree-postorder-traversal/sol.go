package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func postorderTraversalInPlace(root *TreeNode, list *[]int) {
	if root == nil {
		return
	}
	postorderTraversalInPlace(root.Left, list)
	postorderTraversalInPlace(root.Right, list)
	*list = append(*list, root.Val)
}

func postorderTraversal(root *TreeNode) []int {
	list := &[]int{}
	postorderTraversalInPlace(root, list)
	return *list
}
