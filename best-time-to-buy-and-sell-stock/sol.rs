impl Solution {
    pub fn max_profit(prices: Vec<i32>) -> i32 {
        prices
            .iter()
            .fold((prices[0], 0), |(min, max_diff), &price| {
                if price < min {
                    return (price, max_diff);
                }
                (min, max_diff.max(price - min))
            }).1
    }
}