package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeNthFromEnd(head *ListNode, n int) *ListNode {
	top := &ListNode{Val: 0, Next: head}
	parent := top
	for current, c := head, 1; current != nil; current, c = current.Next, c+1 {
		if c > n {
			parent = parent.Next
		}
	}
	parent.Next = parent.Next.Next
	return top.Next
}
