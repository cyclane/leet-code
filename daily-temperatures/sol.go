package main

func dailyTemperatures(temperatures []int) []int {
	out := make([]int, len(temperatures))
	stack := make([][2]int, 0)
	l := 0
	for i := range temperatures {
		if l != 0 && temperatures[i] > stack[l-1][1] {
			for l != 0 && temperatures[i] > stack[l-1][1] {
				out[stack[l-1][0]] = i - stack[l-1][0]
				stack = stack[:l-1]
				l--
			}
		}
		stack = append(stack, [2]int{i, temperatures[i]})
		l++
	}
	return out
}
