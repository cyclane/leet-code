class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        exists = {}
        for n in nums:
            if n in exists:
                return True
            exists[n] = True
        return False