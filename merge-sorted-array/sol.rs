impl Solution {
    pub fn merge(nums1: &mut Vec<i32>, m: i32, nums2: &mut Vec<i32>, n: i32) {
        let mut position_1 = m as usize;
        let mut insert_position = position_1 + n as usize;
        while !nums2.is_empty() {
            insert_position -= 1;
            let last = *nums2.last().unwrap();
            let max = if position_1 == 0 { last } else { nums1[position_1-1].max(last) };
            if last == max {
                nums2.pop();
            } else {
                position_1 -= 1;
            }
            nums1[insert_position] = max;
        }
    }
}