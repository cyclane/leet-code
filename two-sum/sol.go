package main

// Time: O(n)
// Space: O(n)
func twoSum(nums []int, target int) []int {
	exists := make(map[int]int)
	for i1, num := range nums {
		if i2, ok := exists[target-num]; ok && i1 != i2 {
			return []int{i1, i2}
		}
		exists[num] = i1
	}
	return []int{}
}
