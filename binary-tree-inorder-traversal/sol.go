package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversalInPlace(root *TreeNode, list *[]int) {
	if root == nil {
		return
	}
	inorderTraversalInPlace(root.Left, list)
	*list = append(*list, root.Val)
	inorderTraversalInPlace(root.Right, list)
}

func inorderTraversal(root *TreeNode) []int {
	list := &[]int{}
	inorderTraversalInPlace(root, list)
	return *list
}
