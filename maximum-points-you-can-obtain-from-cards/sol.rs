impl Solution {
    pub fn max_score(card_points: Vec<i32>, k: i32) -> i32 {
        let mut sum = card_points.iter()
            .take(k as usize)
            .sum::<i32>();
        let mut max = sum;
        for (sub, add) in card_points.iter()
            .take(k as usize)
            .rev()
            .zip(
                card_points.iter()
                    .rev()
                    .take(k as usize)
            ) {
            sum += add - sub;
            max = max.max(sum);
        }
        max
    }
}