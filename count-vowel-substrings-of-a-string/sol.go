package main

func vowel(char rune) (int, bool) {
	v, ok := map[rune]int{
		'a': 0b00001,
		'e': 0b00010,
		'i': 0b00100,
		'o': 0b01000,
		'u': 0b10000,
	}[char]
	return v, ok
}

func countVowelSubstrings(word string) int {
	order := make([]int, 0)
	counts := make(map[int]int)
	last, m, count := 0, 1, 0
	for _, char := range word {
		if last == 0b11111 {
			count += m
		}
		vow, ok := vowel(char)
		if !ok {
			order = make([]int, 0)
			counts = make(map[int]int)
			last, m = 0, 1
		} else {
			for len(order) > 0 && (order[0] == vow || counts[order[0]] > 1) {
				counts[order[0]]--
				order = order[1:]
				m++
			}
			last |= vow
			order = append(order, vow)
			counts[vow]++
		}
	}
	if last == 0b11111 {
		count += m
	}
	return count
}
