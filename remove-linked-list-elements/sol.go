package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func removeElements(head *ListNode, val int) *ListNode {
	top := &ListNode{Val: 0, Next: head}
	last := top
	for current := head; current != nil; current = current.Next {
		if current.Val == val {
			last.Next = current.Next
		} else {
			last = current
		}
	}
	return top.Next
}
