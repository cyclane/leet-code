package main

func minStartValue(nums []int) int {
	min, s := nums[0], 0
	for i := range nums {
		s += nums[i]
		if s < min {
			min = s
		}
	}
	if min > 0 {
		return 1
	}
	return 1 - min
}
