package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func preorderTraversalInPlace(root *TreeNode, list *[]int) {
	if root == nil {
		return
	}
	*list = append(*list, root.Val)
	preorderTraversalInPlace(root.Left, list)
	preorderTraversalInPlace(root.Right, list)
}

func preorderTraversal(root *TreeNode) []int {
	list := &[]int{}
	preorderTraversalInPlace(root, list)
	return *list
}
