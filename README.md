# Gleb's leetcode solutions
These are most of my solutions to various
[leetcode](https://leetcode.com) problems.

They are by no means the best solutions, rather
what I could come up with by myself.

In each solution I always aim for the lowest
possible time complexity first; however, there
may be multiple solutions for some problems to
try to minimise space complexity instead.

## Naming
Each folder is the name of the problem from its URL,
so the problem can be accessed at:
```
https://leetcode.com/problems/<folder-name>/
```

All solutions for the various languages are named
`sol.<ext>`, although generally I don't vary all that
much.

If I have written multiple solutions in a language,
they will be named `sol_<description>.<ext>`.

## Included & not included code
This is very inconsistent, but generally I will
include a `package main` in .go files, as well as 
any extra struct / class definitions in all files.
Although a lot of the time I also don't do this.