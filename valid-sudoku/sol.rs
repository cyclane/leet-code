use std::collections::{HashMap, HashSet};

// TODO: Broken
impl Solution {
    pub fn is_valid_sudoku(board: Vec<Vec<char>>) -> bool {
        let mut rows: HashMap<usize, HashSet<char>> = HashMap::new();
        let mut cols: HashMap<usize, HashSet<char>> = HashMap::new();
        let mut sqrs: HashMap<usize, HashSet<char>> = HashMap::new();

        for row in (0..9usize) {
            rows.insert(row, HashSet::new());
            for col in (0..9usize) {
                if !cols.contains_key(&col) {
                    cols.insert(col, HashSet::new());
                }
                let cell = *board.get(row).unwrap().get(col).unwrap();
                if cell != '.' {
                    if !rows.get_mut(&row).unwrap().insert(cell) || !cols.get_mut(&row).unwrap().insert(cell) {
                        return false;
                    }
                    let sqr = row - (row % 3) + col / 3;
                    if !sqrs.contains_key(&sqr) {
                        sqrs.insert(sqr, HashSet::new());
                    }
                    if !sqrs.get_mut(&sqr).unwrap().insert(cell) {
                        return false;
                    }
                }
            }
        }

        true
    }
}