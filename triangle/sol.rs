impl Solution {
    pub fn minimum_total(mut triangle: Vec<Vec<i32>>) -> i32 {
        for row in (0..triangle.len() - 1).rev() {
            for idx in 0..row + 1 {
                triangle[row][idx] += triangle[row + 1][idx].min(triangle[row + 1][idx + 1]);
            }
        }
        triangle[0][0]
    }
}