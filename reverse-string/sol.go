package main

// Time: O(n)
// Space: O(1)
func reverseString(s []byte) {
	l := len(s)
	for p := 0; p <= (l-1)/2; p++ {
		tmp := s[p]
		s[p] = s[l-p-1]
		s[l-p-1] = tmp
	}
}
