impl Solution {
    pub fn min_partitions(n: String) -> i32 {
        (n.bytes()
            .max()
            .unwrap() - b'0') as i32
    }
}