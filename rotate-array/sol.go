package main

// Time: O(n)
// Space: O(1) (original array modified)
func rotate(nums []int, k int) {
	c, a, l, idx, num := 0, 0, len(nums), 0, nums[0]
	for c != l {
		if idx == a && c != 0 {
			a++
			idx = a
			num = nums[idx]
		}
		new_idx := (idx + k) % l
		old_num := nums[new_idx]
		nums[new_idx] = num
		idx, num = new_idx, old_num
		c++
	}
}
