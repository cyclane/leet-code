package main

// Time: O(n)
// Space: O(1)
func moveZeroes(nums []int) {
	zeroes := 0
	for idx, num := range nums {
		if num == 0 {
			zeroes++
		} else {
			nums[idx-zeroes] = num
		}
	}
	for idx := len(nums) - 1; idx >= len(nums)-zeroes; idx-- {
		nums[idx] = 0
	}
}
