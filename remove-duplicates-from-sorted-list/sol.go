package main

type ListNode struct {
	Val  int
	Next *ListNode
}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func deleteDuplicates(head *ListNode) *ListNode {
	for current := head; current != nil; current = current.Next {
		for current.Next != nil && current.Val == current.Next.Val {
			current.Next = current.Next.Next
		}
	}
	return head
}
