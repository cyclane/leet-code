use std::collections::HashSet;

impl Solution {
    pub fn maximum_unique_subarray(nums: Vec<i32>) -> i32 {
        let mut l = 0;
        let mut max = 0;
        let mut sum = 0;
        let mut exists = HashSet::new();
        for r in 0..nums.len() {
            sum += nums[r];
            while !exists.insert(nums[r]) {
                exists.remove(&nums[l]);
                sum -= nums[l];
                l += 1;
            }
            max = max.max(sum);
        }
        max as i32
    }
}