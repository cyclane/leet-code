impl Solution {
    pub fn check_possibility(nums: Vec<i32>) -> bool {
        nums.windows(2)
            .scan(i32::MIN, |max, n| {
                *max = n[0].max(*max);
                Some(n[1] < *max)
            })
            .filter(|&result| result)
            .count() <= 1 ||
        nums.windows(2)
            .rev()
            .scan(i32::MAX, |min, n| {
                *min = n[1].min(*min);
                Some(n[0] > *min)
            })
            .filter(|&result| result)
            .count() <= 1
    }
}