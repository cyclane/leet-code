use std::collections::HashSet;

impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        let bytes = s.as_bytes();
        let mut exists = HashSet::new();
        let mut max = 0;
        let mut p1 = 0;
        for p2 in 0..s.len() {
            while !exists.insert(bytes[p2]) {
                exists.remove(&bytes[p1]);
                p1 += 1;
            }
            max = max.max(exists.len());
        }
        max as i32
    }
}