package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func insertIntoBST(root *TreeNode, val int) *TreeNode {
	if root == nil {
		return &TreeNode{
			Val: val,
		}
	}
	node := &TreeNode{
		Val: val,
	}
	current := root
	for {
		if val < current.Val {
			if current.Left == nil {
				current.Left = node
				return root
			}
			current = current.Left
		} else {
			if current.Right == nil {
				current.Right = node
				return root
			}
			current = current.Right
		}
	}
}
