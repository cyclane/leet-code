package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func levelOrder(root *TreeNode) [][]int {
	list := [][]int{}
	queue := []*TreeNode{root}
	for {
		current := []int{}
		current_queue := queue
		queue = queue[len(queue):]
		for _, node := range current_queue {
			if node != nil {
				current = append(current, node.Val)
				queue = append(queue, node.Left, node.Right)
			}
		}
		if len(current) == 0 {
			break
		}
		list = append(list, current)
	}
	return list
}
