package main

type ListNode struct {
	Val  int
	Next *ListNode
}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func hasCycle(head *ListNode) bool {
	exists := map[*ListNode]bool{}
	current := head
	for current != nil {
		exists[current] = true
		current = current.Next
		if exists[current] {
			return true
		}
	}
	return false
}
