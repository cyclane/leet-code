/**
 * @param {number[]} nums
 * @return {boolean}
 */
var containsDuplicate = function(nums) {
    let exists = {};
    return nums.some(n => {
        if (exists[n]) {
            return true;
        }
        exists[n] = true;
        return false;
    });
};