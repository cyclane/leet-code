use std::collections::HashMap;

impl Solution {
    pub fn can_construct(ransom_note: String, magazine: String) -> bool {
        let mut magazine_map = magazine.bytes()
            .fold([0; 26], |mut map, chr| {
                map[chr as usize - 97] += 1;
                map
            });
        ransom_note.bytes()
            .all(|chr| {
                let old = magazine_map[chr as usize - 97];
                if old == 0 {
                    return false;
                }
                magazine_map[chr as usize - 97] = old - 1;
                true
            })
    }
}