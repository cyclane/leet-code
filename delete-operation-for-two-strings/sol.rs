struct Solution{}
// Copied this from the solutions tab pretty much since I couldn't
// solve the problem myself in under O(n^2) (Time Limit Exceeded)
// Good learning problem though
impl Solution {
    pub fn min_distance(word1: String, word2: String) -> i32 {
        let mut dp = vec![0; word2.len() + 1];
        let (w1, w2) = (word1.chars().collect::<Vec<_>>(), word2.chars().collect::<Vec<_>>());
        for i in 0..word1.len() + 1 {
            let mut tmp = vec![0; word2.len() + 1];
            for j in 0..word2.len() + 1 {
                if (i == 0 || j == 0) {
                    tmp[j] = i + j;
                } else if (w1[i - 1] == w2[j - 1]) {
                    tmp[j] = dp[j - 1];
                } else {
                    tmp[j] = 1 + dp[j].min(tmp[j - 1]);
                }
            }
            dp = tmp;
        }
        dp[word2.len()] as i32
    }
}

fn main() {
    println!("{:?}", Solution::min_distance(String::from("ebvivhpfxoptspwianmuhmkmbhxkqbrbgpfwpfcjixzhsjmtsgrzfshvkrvoxvjpmmsrojnpgzqdyofvicscopak"), String::from("vxoumkmxbpcixzhtrfhxmnzqyvisp")));
}