package main

func isBadVersion(version int) bool

// Time: O(log(n))
// Space: O(1)
func firstBadVersion(n int) int {
	move := 0
	for n > 1 {
		p := n / 2
		if isBadVersion(move + p) {
			n = p
		} else {
			n = n - p
			move += p
		}
	}
	return move + n
}
