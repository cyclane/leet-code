#!/bin/sh

grep -oE "[a-z]+" words.txt | sort -u | xargs -I{} sh -c 'echo {} $(grep -oP "(?<![a-z]){}(?![a-z])" words.txt | wc -w)' | sort -nr -t " " -k 2
