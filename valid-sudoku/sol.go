func isValidSudoku(board [][]byte) bool {
    rows := make(map[int]map[byte]bool)
    cols := make(map[int]map[byte]bool)
    sqrs := make(map[int]map[byte]bool)
    var cell byte
    
    for rowIdx := 0; rowIdx < 9; rowIdx++ {
        rows[rowIdx] = make(map[byte]bool)
        for colIdx := 0; colIdx < 9; colIdx++ {
            if cols[colIdx] == nil{
                cols[colIdx] = make(map[byte]bool)
            }
            cell = board[rowIdx][colIdx]
            if cell != '.' {
                if rows[rowIdx][cell] || cols[colIdx][cell] {
                    return false
                }
                sqr := rowIdx - (rowIdx % 3) + colIdx / 3
                if sqrs[sqr] == nil {
                    sqrs[sqr] = make(map[byte]bool)
                }
                if sqrs[sqr][cell] {
                    return false
                }
                rows[rowIdx][cell], cols[colIdx][cell], sqrs[sqr][cell] = true, true, true
            }
        }
    }
    
    return true
}