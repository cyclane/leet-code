package main

type ListNode struct {
	Val  int
	Next *ListNode
}

func middleNode(head *ListNode) *ListNode {
	mid, end := head, head
	for c := 0; end != nil; c++ {
		end = end.Next
		if c%2 == 1 {
			mid = mid.Next
		}
	}
	return mid
}
