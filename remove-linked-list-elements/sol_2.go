package main

type ListNode struct {
	Val  int
	Next *ListNode
}

// Another solution I did because I accidentally
// did the question again from scratch
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func removeElements(head *ListNode, val int) *ListNode {
	top_head := &ListNode{
		Val:  -1,
		Next: head,
	}
	current := top_head
	for current != nil && current.Next != nil {
		if current.Next.Val == val {
			current.Next = current.Next.Next
		} else {
			current = current.Next
		}
	}
	return top_head.Next
}
