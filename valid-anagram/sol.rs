// For unicode hashmaps may be used
impl Solution {
    pub fn is_anagram(s: String, t: String) -> bool {
        let mut s_map = s.bytes()
            .fold([0; 26], |mut map, chr| {
                map[chr as usize - 97] += 1;
                map
            });
        t.bytes()
            .fold(s_map, |mut map, chr| {
                map[chr as usize - 97] -= 1;
                map
            })
            .iter()
            .all(|&count| count == 0)
    }
}