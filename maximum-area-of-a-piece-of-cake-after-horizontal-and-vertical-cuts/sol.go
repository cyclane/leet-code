package main

import "sort"

const M = 1000000007

func maxArea(h int, w int, horizontalCuts []int, verticalCuts []int) int {
	sort.Ints(horizontalCuts)
	sort.Ints(verticalCuts)

	hs, vs := len(horizontalCuts), len(verticalCuts)
	maxH, maxW := horizontalCuts[0], verticalCuts[0]
	horizontalCuts, verticalCuts = append(horizontalCuts, h), append(verticalCuts, w)
	for i := 0; i < hs; i++ {
		d := horizontalCuts[i+1] - horizontalCuts[i]
		if d > maxH {
			maxH = d
		}
	}
	for i := 0; i < vs; i++ {
		d := verticalCuts[i+1] - verticalCuts[i]
		if d > maxW {
			maxW = d
		}
	}
	return (maxW * maxH) % M
}
