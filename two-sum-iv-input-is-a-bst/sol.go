package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func search(node *TreeNode, k int, visited *map[int]bool) bool {
	if node == nil {
		return false
	}
	if _, ok := (*visited)[k-node.Val]; ok {
		return true
	}
	(*visited)[node.Val] = true
	return search(node.Left, k, visited) || search(node.Right, k, visited)
}

func findTarget(root *TreeNode, k int) bool {
	visited := map[int]bool{}
	return search(root, k, &visited)
}
