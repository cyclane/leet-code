package main

import "sort"

func maximumUnits(boxTypes [][]int, truckSize int) int {
	sort.Slice(boxTypes, func(i, j int) bool {
		return boxTypes[i][1] > boxTypes[j][1]
	})
	units := 0
	count := 0
	for idx := 0; idx < len(boxTypes); idx++ {
		units += boxTypes[idx][0] * boxTypes[idx][1]
		count += boxTypes[idx][0]
		if count >= truckSize {
			units -= (count - truckSize) * boxTypes[idx][1]
			return units
		}
	}
	return units
}
