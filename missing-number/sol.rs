impl Solution {
    pub fn missing_number(nums: Vec<i32>) -> i32 {
        let expected_sum = (nums.len() * (nums.len() + 1) / 2) as i32;
        nums
            .iter()
            .fold(expected_sum, |acc, x| acc - *x)
    }
}