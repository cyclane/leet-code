package main

func reverseWords(s string) string {
	l := len(s)
	out := make([]rune, l)
	word := make([]rune, 0)
	for idx, char := range s {
		if char == ' ' {
			for widx, wchar := range word {
				out[idx-widx-1] = wchar
			}
			out[idx] = ' '
			word = word[:0]
		} else {
			word = append(word, char)
		}
	}
	for widx, wchar := range word {
		out[l-widx-1] = wchar
	}
	return string(out)
}
