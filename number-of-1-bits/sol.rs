impl Solution {
    pub fn hammingWeight (n: u32) -> i32 {
        (0..32).into_iter()
            .fold(0, |acc, p| {
                let compare_bit = 1 << p;
                if n & compare_bit == compare_bit {
                    return acc + 1;
                }
                acc
            })
    }
}