// TODO : Tidy up?? This is quite the unreadable mess at the moment
impl Solution {
    pub fn search(board: Vec<Vec<i32>>) -> i32 {
        if board.len() == 1 {
            return board[0].len();
        }
        board[0].iter()
            .map(|&current|
                Solution::search(
                    board[1..].iter()
                        .enumerate()
                        .map(|(idx, row)| {
                            row.iter()
                                .filter_map(|n| if *n != current &&
                                    *n != current - idx as i32 - 1 &&
                                    *n != current + idx as i32 + 1 {
                                    Some(*n)
                                } else { None })
                                .collect()
                        })
                        .collect()
                )
            )
            .sum()
    }
    pub fn total_n_queens(n: i32) -> Vec<Vec<String>> {
        Solution::search(vec![(0..n).collect(); n as usize])
    }
}