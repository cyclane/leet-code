package main

import "math"

// Time: O(1) (assuming computer has float64 sqrt instruction)
// Space: O(1)
func arrangeCoins(n int) int {
	return int(math.Sqrt(float64(2*n)+0.25) - 0.5)
}
