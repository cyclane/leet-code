impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack = vec![];
        s.chars()
            .all(|chr| match chr {
                '(' | '{' | '[' => {
                    stack.push(chr);
                    true
                },
                _ => {
                    let opened = stack.pop()
                        .unwrap_or(' ');
                    // Hacky way to check if the brackets match. Not scalable, but works here.
                    if chr as u8 / 10 != opened as u8 / 10 {
                        return false;
                    }
                    true
                } 
            }) && stack.len() == 0
    }
}