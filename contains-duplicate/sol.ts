function containsDuplicate(nums: number[]): boolean {
    let exists = {};
    return nums.some(n => {
        if (exists[n]) {
            return true;
        }
        exists[n] = true;
        return false;
    });
};