package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// Time: O(n)
// Space: O(1)
func sumOfLeftLeaves(root *TreeNode) int {
	return soll(root, false)
}

func soll(root *TreeNode, include bool) int {
	if root == nil {
		return 0
	}
	if root.Left == nil && root.Right == nil && include {
		return root.Val
	}
	return soll(root.Left, true) + soll(root.Right, false)
}
