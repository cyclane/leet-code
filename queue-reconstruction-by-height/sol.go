package main

import "sort"

func reconstructQueue(people [][]int) [][]int {
	sort.Slice(people, func(i, j int) bool {
		return people[i][0] > people[j][0] || (people[i][0] == people[j][0] &&
			people[i][1] < people[j][1])
	})
	for idx, p := range people {
		copy(people[p[1]+1:idx+1], people[p[1]:idx+1])
		people[p[1]] = p
	}
	return people
}
