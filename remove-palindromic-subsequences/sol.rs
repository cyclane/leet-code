impl Solution {
    pub fn remove_palindrome_sub(s: String) -> i32 {
        if s.bytes()
            .rev()
            .zip(s.bytes())
            .all(|(b1, b2)| b1 == b2) {
            1
        } else {
            2
        }
    }
}