impl Solution {
    pub fn max_sub_array(nums: Vec<i32>) -> i32 {
        nums.iter()
            .fold((nums[0], 0), |(max, sum), n| {
                let new_sum = sum + n;
                (max.max(new_sum), 0.max(new_sum))
            })
            .0
    }
}