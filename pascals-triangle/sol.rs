impl Solution {
    pub fn generate(num_rows: i32) -> Vec<Vec<i32>> {
        let mut rows = vec![vec![1]];
        for row_idx in (1usize..num_rows as usize) {
            // Could do this with .window(2), adding the two elements of each window,
            // but that's a bit fussy in this case I think.
            let mut row = vec![1];
            for idx in (0usize..(rows[row_idx-1].len()-1)) {
                row.push(rows[row_idx-1][idx]+rows[row_idx-1][idx+1]);
            }
            row.push(1);
            rows.push(row);
        }
        rows
    }
}